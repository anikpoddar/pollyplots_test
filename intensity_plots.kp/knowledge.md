---
title: Plots from El_Maven (Intensity plot and Heatmap)
authors:
- Anik_Poddar
tags:
- Plots
- Polly
- Knowledge_repo
created_at: 2018-08-10 00:00:00
updated_at: 2018-08-10 14:15:48.773665
tldr: Using Knowledge_repo v0.76 and uploadiing in polly
thumbnail: images/unnamed-chunk-1-1.png
---
```r
  library(ggplot2)
  library(easyGgplot2)
  data_set <- read.csv('dataset.csv')
  #df = apply(data_set, 1, function(r) any(r %in% c('UDP-D-glucose')))
  df = which(data_set$compound=='N-acetyl-glucosamine-1/6-phosphate')
  selected = data_set[df,]
  required_data <- data.frame(selected$isotopeLabel,selected[ , grepl( "X091215" , names( selected ) ) ])
  temp_data <- required_data
  required_data$selected.isotopeLabel <- NULL

  
  #ggplot(required_data,aes(required_data[1,],fill = required_data[2,]))+
           #geom_bar( stat="identity", position="fill")
  
  #ggplot2.barplot(data=required_data, xName="Plots",
                #groupName=required_data["selected.isotopeLabel"])
  barplot(as.matrix(required_data), col = rainbow(16), names.arg = c("240M","240I","120M","120I","45M","45I","15M","15I","5M","5I","3M","3I","1M","1I","0M","0I"))
```

![plot of chunk unnamed-chunk-1](images/unnamed-chunk-1-1.png)
